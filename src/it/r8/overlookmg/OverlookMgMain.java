package it.r8.overlookmg;
import java.util.List;
import java.util.Scanner;

import it.r8.overlookmg.domain.Booking;
import it.r8.overlookmg.domain.BookingDto;
import it.r8.overlookmg.domain.Guest;
import it.r8.overlookmg.domain.Room;
import it.r8.overlookmg.repository.BookingRepository;
import it.r8.overlookmg.repository.GuestRepository;
import it.r8.overlookmg.repository.MysqlBookingRepository;
import it.r8.overlookmg.repository.MysqlGuestRepository;
import it.r8.overlookmg.repository.MysqlRoomRepository;
import it.r8.overlookmg.repository.RoomRepository;

public class OverlookMgMain {
	public static void main(String[] args) {
		Log_in();
	}	
	public static void Log_in(){
		try (Scanner input = new Scanner(System.in)){
			String username ="r8";
			String password ="123";
			System.out.print("Enter Username : ");
			String username1= input.next();
			System.out.print("Enter password : ");
			String password1=input.next();
	
			if (username.equals(username1)&&password.equals(password1)){
				showFirstMenu();
			}
			else {
				System.out.println("Invalid Username or Password! ");
				Log_in();
			}
		}
	}
	
	public static void showFirstMenu() {	
		try (Scanner input = new Scanner(System.in)) {
			String c= "" ;
			System.out.print("|      MAIN MENU     |\n");
			System.out.print(" ____________________\n");
			System.out.print("|      1 guest       |\n"
					+ 		 "|      2 booking     |\n"
					+ 		 "|      3 room        |\n"
					+        "|      4 exit        |\n"
					+		 " ____________________\n"); 
			
			while (input.hasNextInt()){
				
				c=input.next();
				switch(c) {
					case "1": 
						showGuestMenu();
					break;
					case "2":
						showBookingMenu();
					break;	
					case "3":
						showRoomMenu();
					break;
					case "4": 
						System.out.println("Terminated ");
						System.exit(1);
						break;	
					default:
						System.out.println("Error !!!");
				}
			}
		}
	}
	
	public static void showGuestMenu() {	
		try (Scanner input = new Scanner(System.in)) {
			GuestRepository guestRepository = new MysqlGuestRepository();
			Guest guest= new Guest();
			String c= "" ;
			System.out.print("|       SELECT       |\n ");
			System.out.print("____________________\n");
			System.out.print("|  1 Add guest       |\n"
					+        "|  2 View all guests |\n"
					+ 		 "|  3 Update guest    |\n"
					+ 		 "|  4 Delete guest    |\n"
					+        "|  5 Return          |\n"
					+		 " ____________________\n");
			//while (input.hasNextInt()){	
				c= input.next();
				if (c.equals("1") || c.equals("3")) {
					System.out.print("Insert name : ");  
					guest.setName(input.next());
					System.out.print("Insert surname : ");
					guest.setSurname(input.next());
					System.out.print("Insert gender M/F : ");
					guest.setGender(input.next());
					System.out.print("Insert identification : ");
					guest.setIdentification(input.next());
					System.out.print("Insert nation : ");
					guest.setNation(input.next());
					System.out.print("Insert city : ");
					guest.setCity(input.next());
				}
				switch (c){
					case "1" : //Aggiunta del cliente
						guestRepository.add(guest);
						System.out.println("Guest registration successfully completed");
						showGuestMenu();
						
					break;
					case "2" : // Visualizza 	clienti
						List<Guest> guests = guestRepository.getAll();
						for(Guest guestReaded : guests) {
							System.out.println(guestReaded);
						}
						showGuestMenu();
					break;
					case "3" : //Aggiorna cliente
						System.out.print("Insert Id: ");
						guestRepository.update(input.nextInt(), guest);
						showGuestMenu();
					break;
					case "4": //Cancella cliente
						System.out.print("Insert Id: ");
						guestRepository.delete(input.nextInt());
						showGuestMenu();
					break;
					case "5": 
						showFirstMenu();
					break;
					default:
						System.out.println("Error !!!");
				}
			//}
			
		}
	}
	
	public static void showRoomMenu() {	
		try (Scanner input = new Scanner(System.in)) {
			RoomRepository roomRepository = new MysqlRoomRepository();
			Room room= new Room();
			String c= "" ;
			System.out.print("select \n1: Add room\n2: View all rooms\n3: Update room\n4: Delete\n5: Return\n");
			//while (input.hasNextInt()){	
				c= input.next();
				if (c.equals("1") || c.equals("3")) {
					System.out.print("Insert number room : ");  
					room.setNumber_room(input.nextInt());
					System.out.print("Insert type : ");
					room.setType(input.next());
					System.out.print("Insert price : ");
					room.setPrice(input.nextDouble());
				}
				switch (c){
					case "1" : //Aggiunta camera
						roomRepository.add(room);
						System.out.println("Room registration successfully completed");
						showRoomMenu();
						
					break;
					case "2" : // Visualizza camere
						List<Room> rooms = roomRepository.getAll();
						for(Room roomReaded : rooms) {
							System.out.println(roomReaded);
						}
						showRoomMenu();
					break;
					case "3" : //Aggiorna camera
						System.out.print("Insert Id: ");
						roomRepository.update(input.nextInt(), room);
						showRoomMenu();
					break;
					case "4": //Cancella camera
						System.out.print("Insert Id: ");
						roomRepository.delete(input.nextInt());
						System.out.print("Room deleted successfully completed");
						showRoomMenu();
					break;
					case "5": 
						showFirstMenu();
					break;
					default:
						System.out.println("Error !!!");
				}
		}
	}
	public static void showBookingMenu() {	
		try (Scanner input = new Scanner(System.in)) {
			BookingRepository bookingRepository = new MysqlBookingRepository();
			Booking booking= new Booking();
			String c= "" ;
			System.out.print("select \n1: Add booking\n2: View all bookings\n3: Update booking\n4: Delete booking\n5: Return\n");
			//while (input.hasNextInt()){	
				c= input.next();
				if (c.equals("1") || c.equals("3")) {
					System.out.print("Insert  checkin: ");  
					booking.setCheckin(input.next());
					System.out.print("Insert checkout : ");
					booking.setCheckout(input.next());
					System.out.print("Insert cost : ");
					booking.setCost(input.nextDouble());
					System.out.print("Insert id room : ");
					booking.setIdroom(input.nextInt());
					System.out.print("Insert id guest : ");
					booking.setIdguest(input.nextInt());
				}
				switch (c){
					case "1" : //Aggiunta camera
						bookingRepository.add(booking);
						System.out.println("Booking registration successfully completed");
						showBookingMenu();
						
					break;
					case "2" : // Visualizza camere
						List<BookingDto> bookings = bookingRepository.getAlljoin();
						for(BookingDto bookingReaded : bookings) {
							System.out.println(bookingReaded);
						}
						showBookingMenu();
					break;
					case "3" : //Aggiorna camera
						System.out.print("Insert Id: ");
						bookingRepository.update(input.nextInt(), booking);
						showBookingMenu();
					break;
					case "4": //Cancella camera
						System.out.print("Insert Id: ");
						bookingRepository.delete(input.nextInt());
						System.out.print("Booking deleted successfully completed");
						showBookingMenu();
					break;
					case "5": 
						showFirstMenu();
					break;
					default:
						System.out.println("Error !!!");
				}
		}
	}
}

	
	
	
	
	
	







