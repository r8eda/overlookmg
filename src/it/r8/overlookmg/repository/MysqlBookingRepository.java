package it.r8.overlookmg.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.r8.overlookmg.domain.Booking;
import it.r8.overlookmg.domain.BookingDto;
import it.r8.overlookmg.domain.Guest;
import it.r8.overlookmg.domain.Room;






public class MysqlBookingRepository implements BookingRepository{
	private static final String URL = "jdbc:mysql://localhost:3306/overlook";
	private static final String USER = "root";
	private static final String PASSWORD = "test";
	
	/*INSERT*/
	public void add(Booking booking) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format (Locale.US,"INSERT INTO Booking(checkin , checkout,  cost, idroom, idguest) " + 
										  "VALUES ('%s', '%s', '%f' , '%d','%d')" ,booking.getCheckin(),
										   booking.getCheckout(), booking.getCost(), booking.getIdroom(),booking.getIdguest());
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}

	
	public List<BookingDto> getAlljoin() {
    	List<BookingDto> bookingsDto = new ArrayList<BookingDto>();
    	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
    		Statement statement = connection.createStatement();
		    ResultSet resultSet = statement.executeQuery("SELECT booking.*, guest.*, room.* FROM booking inner join guest on booking.idguest=guest.id inner join room on booking.idroom=room.id WHERE booking.deleted=0");
		    
		    
		    
		    while (resultSet.next()) {
			    BookingDto bookingDto = new BookingDto();
			    Guest guest = new Guest();
			    Room room = new Room();
			    room.setId(resultSet.getInt("room.id"));
			    room.setNumber_room(resultSet.getInt("number_room"));
				room.setType(resultSet.getString("type"));
				room.setPrice(resultSet.getDouble("price"));
				
				guest.setId(resultSet.getInt("id"));
				guest.setName(resultSet.getString("name"));
				guest.setSurname(resultSet.getString("surname"));
				guest.setGender(resultSet.getString("gender"));
				guest.setIdentification(resultSet.getString("identification"));
				guest.setNation(resultSet.getString("nation"));
				guest.setCity(resultSet.getString("city"));
				
			    bookingDto.setId(resultSet.getInt("booking.id"));
			    bookingDto.setCheckin(resultSet.getString("booking.checkin"));
			    bookingDto.setCheckout(resultSet.getString("booking.checkout"));
			    bookingDto.setCost(resultSet.getDouble("booking.cost"));
			    bookingDto.setRoom(room);
			    bookingDto.setGuest(guest);
			    bookingsDto.add(bookingDto);
		   } 
	   }
	   catch (SQLException e) {
			System.out.println("SQL exception");
	   }
	   return bookingsDto;
    }
	
	
/*SELECT*/
    public List<Booking> getAll() {
    	List<Booking> bookings = new ArrayList<Booking>();
    	try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
    		Statement statement = connection.createStatement();
		    ResultSet resultSet = statement.executeQuery("Select * from booking");
		    while (resultSet.next()) {
			    Booking booking = new Booking();
			    booking.setId(resultSet.getInt("id"));
			    booking.setCheckin(resultSet.getString("checkin"));
			    booking.setCheckout(resultSet.getString("checkout"));
			    booking.setCost(resultSet.getDouble("cost"));
			    booking.setIdroom(resultSet.getInt("idroom"));
			    booking.setIdguest(resultSet.getInt("idguest"));
			    bookings.add(booking);
		   } 
	   }
	   catch (SQLException e) {
			System.out.println("SQL exception");
	   }
	   return bookings;
    }
	/*UPDATE*/
	public void update(int id, Booking booking) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE Booking b" +
									   "  SET checkin = '%s'," +
									   "	  checkout = '%s'," +
									   "      cost = '%f'," +
									   "	  idroom = '%d'," +
									   "      idguest = '%d'," +	
									   "WHERE b.id = %d", booking.getCheckin(),
									   booking.getCheckout(), booking.getCost(),booking.getIdguest(), booking.getIdroom(), id);
			
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
	/*DELETE*/
	public void delete(int id) {
			Booking booking = new Booking();
			booking.setDeleted(false);
			try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE Booking SET deleted =1 WHERE id =%d", id);
			int result = statement.executeUpdate(sql);
			System.out.println(result);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
}
