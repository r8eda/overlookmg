package it.r8.overlookmg.repository;
import java.util.List;
import it.r8.overlookmg.domain.Booking;
import it.r8.overlookmg.domain.BookingDto;


public interface BookingRepository {

		List<Booking> getAll();
		
		List<BookingDto> getAlljoin();
		
		void add(Booking booking);
		
		void update(int id, Booking booking);
		
		void delete(int id);
	}



