package it.r8.overlookmg.repository;
import java.util.List;

import it.r8.overlookmg.domain.Guest;

public interface GuestRepository {

	List<Guest> getAll();
	
	void add(Guest guest);
	
	void update(int id, Guest guest);
	
	void delete(int id);
}


