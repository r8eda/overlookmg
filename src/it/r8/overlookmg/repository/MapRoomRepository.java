package it.r8.overlookmg.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.r8.overlookmg.domain.Room;

public class MapRoomRepository  implements RoomRepository{
		private Map<Integer, Room> rooms = new HashMap<Integer, Room>();
		
		public void add(Room room) {		
			rooms.put(this.rooms.size(), room);
		}
		
		@Override
		public List<Room> getAll() {
			return (List<Room>)rooms.values();
		}
		
		@Override
		public void update(int id, Room room) {
			this.rooms.put(id, room);
		}

		@Override
		public void delete(int id) {
			this.rooms.remove(id);
		}
	

	}
	
	

