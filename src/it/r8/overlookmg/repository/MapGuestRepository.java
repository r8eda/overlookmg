package it.r8.overlookmg.repository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.r8.overlookmg.domain.Guest;

public class MapGuestRepository implements GuestRepository {

	private Map<Integer, Guest> guests = new HashMap<Integer, Guest>();

	@Override
	public void add(Guest guest) {		
		guests.put(this.guests.size(), guest);
	}
	
	@Override
	public List<Guest> getAll() {
		return (List<Guest>)guests.values();
	}
	
	@Override
	public void update(int id, Guest guest) {
		this.guests.put(id, guest);
	}

	@Override
	public void delete(int id) {
		this.guests.remove(id);
	}
}
