package it.r8.overlookmg.repository;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.r8.overlookmg.domain.Room;

public class MysqlRoomRepository implements RoomRepository {

		private static final String URL = "jdbc:mysql://localhost:3306/overlook";
		private static final String USER = "root";
		private static final String PASSWORD = "test";
		
		/*INSERT*/
		public void add(Room room) {
			try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
				Statement statement = connection.createStatement();
				String sql = String.format(Locale.US,"INSERT INTO Room(number_room, type, price) " + 
											  "VALUES ('%d', '%s', '%f' )" ,room.getNumber_room(),
											  room.getType(), room.getPrice());
				statement.executeUpdate(sql);
				}
			catch (SQLException e) {
				System.out.println("SQL exception");
				e.printStackTrace();
				}
			}
		
		
		
		/*SELECT*/
		public List<Room> getAll() {
			List<Room> rooms = new ArrayList<Room>();
			try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM Room WHERE deleted=0");
				while (resultSet.next()) {
					Room room = new Room();
					room.setId(resultSet.getInt("id"));
				    room.setNumber_room(resultSet.getInt("number_room"));
					room.setType(resultSet.getString("type"));
					room.setPrice(resultSet.getDouble("price"));
					rooms.add(room);
				}
			}
			catch (SQLException e) {
				System.out.println("SQL exception");
			}

			return rooms;
		}
		
		/*UPDATE*/
		public void update(int id, Room room) {
			try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
				Statement statement = connection.createStatement();
				String sql = String.format("UPDATE Room r" +
										   "  SET number_room = '%d'," +
										   "	 type = '%s'," +
										   "      price = '%f'" +
										   "WHERE r.id = %d", room.getNumber_room(),
										  room.getType(), room.getPrice(), id);
				statement.executeUpdate(sql);
				}
		    catch (SQLException e) {
		    	System.out.println("SQL exception");
				e.printStackTrace();
				}
			}
		
		
		
		/*DELETE*/
		public void delete(int id) {
			Room room = new Room();
			room.setDeleted(false);
			try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
				Statement statement = connection.createStatement();
				String sql = String.format("UPDATE Room SET deleted =1 WHERE id =%d", id);
				int result = statement.executeUpdate(sql);
				System.out.println(result);
				}
			catch (SQLException e) {
				System.out.println("SQL exception");
				e.printStackTrace();
			}
		}
	}
	


