package it.r8.overlookmg.repository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.r8.overlookmg.domain.Booking;
import it.r8.overlookmg.domain.BookingDto;

public class MapBookingRepository implements BookingRepository{

	private Map<Integer, Booking> bookings = new HashMap<Integer, Booking>();

	public void add(Booking booking) {		
		bookings.put(this.bookings.size(), booking);
	}

	@Override
	public List<Booking> getAll() {
		return (List<Booking>)bookings.values();
	}

	@Override
	public List<BookingDto> getAlljoin() {
		return null;
	}

	@Override
	public void update(int id, Booking booking) {
		this.bookings.put(id, booking);
	}

	@Override
	public void delete(int id) {
		this.bookings.remove(id);
	}}






