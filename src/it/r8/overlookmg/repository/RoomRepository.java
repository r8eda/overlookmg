package it.r8.overlookmg.repository;
import java.util.List;
import it.r8.overlookmg.domain.Room;

public interface RoomRepository {
	List<Room> getAll();
	void add (Room room);
	void update(int id, Room room);
	void delete(int id);
	}



