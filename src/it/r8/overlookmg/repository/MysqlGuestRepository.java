package it.r8.overlookmg.repository;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import it.r8.overlookmg.domain.Guest;

public class MysqlGuestRepository implements GuestRepository {

	private static final String URL = "jdbc:mysql://localhost:3306/overlook";
	private static final String USER = "root";
	private static final String PASSWORD = "test";
	
	/*INSERT*/
	public void add(Guest guest) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format(Locale.ENGLISH, "INSERT INTO Guest(name, surname, gender, identification, nation, city) " + 
										  "VALUES ('%s', '%s', '%s', '%s','%s','%s')" ,guest.getName(),
										   guest.getSurname(), guest.getGender(), guest.getIdentification(), guest.getNation(), guest.getCity());
			statement.executeUpdate(sql);

		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
	
	/*SELECT*/
	public List<Guest> getAll() {
		List<Guest> guests = new ArrayList<Guest>();

		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM overlook.guest Where deleted =0");

			while (resultSet.next()) {
				Guest guest = new Guest();
				guest.setId(resultSet.getInt("id"));
				guest.setName(resultSet.getString("name"));
				guest.setSurname(resultSet.getString("surname"));
				guest.setGender(resultSet.getString("gender"));
				guest.setIdentification(resultSet.getString("identification"));
				guest.setNation(resultSet.getString("nation"));
				guest.setCity(resultSet.getString("city"));
				guests.add(guest);
			}

		} catch (SQLException e) {
			System.out.println("SQL exception");
		}

		return guests;
	}
	
	/*UPDATE*/
	public void update(int id, Guest guest) {
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE Guest g" +
									   "  SET name = '%s'," +
									   "	  surname = '%s'," +
									   "      gender = '%s'," +
									   "      identification = '%s'," +
									   "      nation = '%s'," +
									   "      city = '%s' " +
									   "WHERE g.id = %d", guest.getName(),
									   guest.getSurname(), guest.getGender(), guest.getIdentification(),
									   guest.getNation(), guest.getCity(), id);
			
			statement.executeUpdate(sql);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
	
	/*DELETE*/
	public void delete(int id) {
		Guest guest = new Guest ();
		guest.setDeleted(false);
		try (Connection connection = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement statement = connection.createStatement();
			String sql = String.format("UPDATE Guest SET deleted =1 WHERE id =%d " ,id);
			int result = statement.executeUpdate(sql);
			System.out.println(result);
			
		} catch (SQLException e) {
			System.out.println("SQL exception");
			e.printStackTrace();
		}
	}
}









