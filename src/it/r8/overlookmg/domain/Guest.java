package it.r8.overlookmg.domain;

public class Guest {
	private int id;
	private String name ;
	private String surname;
	private String gender;
	private String identification;
	private String nation;
	private String city;
	private boolean deleted;
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Guest (){
		}
	
	public Guest(int id, String name, String surname, String gender, String identification, String nation,
			String city) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.gender = gender;
		this.identification = identification;
		this.nation = nation;
		this.city = city;
	}
		public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getIdentification() {
		return identification;
	}
	public void setIdentification(String identification) {
		this.identification = identification;
	}
	public String getNation() {
		return nation;
	}
	public void setNation(String nation) {
		this.nation = nation;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String toString(){
		return this.name +  " " + this.surname + " " + this.gender + " " + this.identification + " " + this.nation + " " + this.city;
	}
			
}
	

