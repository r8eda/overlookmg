package it.r8.overlookmg.domain;

public class Room {

	private int id;
	private int number_room;
	private String type;
	private double price;
	private boolean deleted;
	public Room (){
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumber_room() {
		return number_room;
	}
	public void setNumber_room(int number_room) {
		this.number_room = number_room;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
		
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public Room(int id, int number_room,String type, double price) {
	
		this.id = id;
		this.type = type;
		this.price = price;
		this.number_room = number_room;
	}
	public String toString(){
		return this.number_room +  " " + this.type + " " + this.price ;
	}
	public boolean isDeleted() {
		return deleted;
	}
	
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
}