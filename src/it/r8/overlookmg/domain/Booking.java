package it.r8.overlookmg.domain;

public class Booking {

	private int id;
	private  String checkin;
	private String checkout;
	private double cost;
	private int idroom;
	private int idguest;
	private boolean deleted;
	
	public Booking () {
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCheckin() {
		return checkin;
	}
	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}
	public String getCheckout() {
		return checkout;
	}
	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
		
	}	
	public int getIdroom() {
		return idroom;
	}
	public void setIdroom(int idroom) {
		this.idroom = idroom;
	}
	public int getIdguest() {
		return idguest;
	}
	public void setIdguest(int idguest) {
		this.idguest = idguest;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public Booking(int id, String checkin, String checkout, double cost, int idguest, int idroom) {
	
		this.id = id;
		this.checkin = checkin;
		this.checkout = checkout;
		this.cost = cost;
		this.idroom=idroom;
		this.idguest=idguest;
	}

	public String toString(){
		return this.checkin + " " + this.checkout + " " + this.cost + " " + this.idroom + " " + this.idguest;
	}
	
	
	
}
