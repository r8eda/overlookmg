package it.r8.overlookmg.domain;

public class BookingDto {

	private int id;
	private  String checkin;
	private String checkout;
	private double cost;
	private Room room;
	private Guest guest;
	private boolean deleted;
	
	public BookingDto() {
		
	}
	
	public BookingDto(int id, String checkin, String checkout, double cost, Room room, Guest guest) {
		
		this.id = id;
		this.checkin = checkin;
		this.checkout = checkout;
		this.cost = cost;
		this.room=room;
		this.guest=guest;
		
	}

	public String toString(){
		return this.checkin + " " + this.checkout + " " + this.cost + " " + this.room + " " + this.guest; 
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCheckin() {
		return checkin;
	}

	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}

	public String getCheckout() {
		return checkout;
	}

	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Guest getGuest() {
		return guest;
	}

	public void setGuest(Guest guest) {
		this.guest = guest;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
}
