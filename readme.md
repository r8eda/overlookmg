﻿**OverLookMG**

Destinatari del Software: Hotel Manager o Receptionists;           
Obiettivo : Gestione della struttura alberghiera
           
![enter image description here](https://res.cloudinary.com/teepublic/image/private/s--vRo1Ps3H--/t_Preview/b_rgb:42332c,c_limit,f_jpg,h_630,q_90,w_630/v1446247562/production/designs/309994_1.jpg)




Struttura del Software nel quale è possibile scegliere 3 diverse sezioni:
  
  1) Gestione ospiti;
  2) Gestione prenotazioni;
  3) Gestione camere;

In "Gestione Ospiti" è possibile aggiungere un nuovo ospite, modificando le credenziali o cancellandolo definitivamente dal sistema ove fosse necessario. 
Inoltre con la funzione "lettura" si andranno a verificare le credenziali dell'ospite o degli ospiti registrati durante la prenotazione.
La "Gestione ospiti" prevede la seguente struttura :  Nome, Cognome, Città, Documento, Genere e Nazione.


In "Gestione prenotazioni" è possibile creare, aggiornare ed eliminare una prenotazione e prevede anche le funzioni di check-in, check-out, durata del soggiorno e costo totale.


In "Gestione camere" è possibile verificare lo status delle camere (libera/occupata), il numero, il prezzo e il tipo. Inoltre ci sarà la possibilità di modificare 
e aggiungere le diverse tipologie di camere che il gestore dell'albergo vorrà mettere a disposizione della clientela.
            


Funzioni disponibili per i gestori del Software:
 - Gestione della Disponibilità/Indisponibilità camere
 - Verifica prenotazione camere
 - Presa in carica delle prenotazione


-------------------------------------------------


Funzioni disponibili per il gestore:

Come gestore posso loggarmi al sistema;
Come gestore posso accedere alle tre sezioni (OSPITI,PRENOTAZIONE,CAMERE);
Come gestore posso controllare lo status delle camere disponibili/indisponibili;
Come gestore posso controllare lo status delle prenotazioni e modificarne lo status (aggiornare,modificare e cancellare);
Come gestore posso effettuare il CHECK-IN e il CHECK-OUT verificandone le credenziali e registrando il pagamento a sistema;
Come gestore posso registrare gli ospiti e controllare (aggiungendo, modificando e cancellando i dati di quest'ultimi);


--------------------------------------------------


"Cliente" sarà rappresentato dai seguenti attributi:
 - Numero Cliente
 - Nome
 - Cognome
 - Genere
 - Identificazione
 - Nazionalità
 - Città


"Prenotazione" sarà rappresentato dai seguenti attributi:
 - Check-In
 - Check-Out
 - Costo


"Camera" sarà rappresentato dai seguenti attributi:
 - Numero 
 - Tipo
 - Prezzo
















